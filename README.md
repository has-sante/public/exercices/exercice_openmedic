# Exercice openmedic 💊

[OpenMedic](https://www.data.gouv.fr/fr/datasets/open-medic-base-complete-sur-les-depenses-de-medicaments-interregimes/) est une base de données ouverte sur les consommations et dépenses de médicaments fournie par l'assurance maladie.


# Problématique métier 

On considère la base openmedic fournit dans le dossier `data` (sous-échantillonnée aux années 2017, 2018), ainsi que le [schéma](https://gitlab.com/healthdatahub/schema-snds/-/blob/ajout_schema_openmedic/schemas/OPENMEDIC/openmedic.json) (au format [Table Schema](https://documentation-snds.health-data-hub.fr/glossaire/table-schema.html)) et les [nomenclatures](https://gitlab.com/healthdatahub/schema-snds/-/tree/ajout_schema_openmedic/nomenclatures/OPENMEDIC) de cette base disponible dans cette MR : https://gitlab.com/healthdatahub/schema-snds/-/merge_requests/110.

Des personnes du métier ne connaissant ni les colonnes, ni les codes des nomenclatures, aimeraient pouvoir poser des questions simples sur ces données, tout en étant le plus proche possible du langage naturel (pas de requêtes SQL). Dans l'idéal, on aimerait pouvoir poser le type de questions suivantes, avec des phrases modulaires et à trous (champs de recherche avancés) : 

- Quel est le nombre d'antithrombotiques prescrits par un pneumologue en 2018 en Auvergne-Rhône-Alpes ?
- Quel est le médicament le plus remboursé chez les femmes en OCCITANIE en 2017 ? 
- Dans quelle région a-t-on délivré le plus d'antidépresseurs en 2018 ? 

# But de l'exercice 🚀

Imaginer et concevoir une solution permettant de répondre à ces questions de manière interactive en restant le plus proche possible du langage naturel. 

La manière de formuler le problème, et le développement de l'API, sont les éléments qui nous intéressent le plus dans le cadre de cet exercice.

Selon le temps, vous pouvez également concevoir les grandes lignes du design d'une interface permettant au métier de poser ses questions et d'en visualiser les réponses, voire en développer un prototype. Il s'agit d'un bonus et ne constitue pas le centre de l'exercice.

# En pratique

Vous pouvez utiliser les technologies de votre choix pour traiter l'exercice.

Le partage de votre travail se fera sur dépôt personnel qui clonera ce dépôt.


# Disclaimer

Ce problème est potentiellement difficile et chronophage.

Nous **n'attendons pas** de solution complète ou très performante.

Il sera bienvenu de simplifier le problème, pour s'attacher à un sous-problème plus simple.
